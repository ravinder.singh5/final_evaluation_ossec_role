variable "vpc_id" {
  type = string
}

variable "region" {
  type = string
}


variable "pvt_subnet_Name" {
  type = list(any)
}


variable "pvt_subnet_CIDR" {
  type = list(any)
}


variable "private_available_zone" {
  type = list(any)
}


variable "additional_tags" {
  default = {}
  type    = map(any)
}

variable "nat_gw_name" {
  type = string
}

variable "ossec_pub_subnet_id" {
  type = string
}

variable "pvt_route_table_name" {
  type = string
}

variable "private_sg_name" {
  type = string
}

variable "ports" {
  type = list(any)
}

variable "protocol" {
  type = string
}

variable "ossec_server_instance_name" {
  type = string
}

variable "instance_ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "key_name" {
  type = string
}

variable "ossec_server_additional_tags" {
  default = {}
  type    = map(any)
}

variable "agent_sg_name" {
  type = string
}

variable "agent_instances_name" {
  type = list(any)
}

variable "ossec_agent_additional_tags" {
  default = {}
  type    = map(any)
}

variable "target_group_name" {
  type = string
}

variable "alb_sg_cidr" {
  type = list(any)
}

variable "alb_name" {
  type = string
}

variable "alb_subnets" {
  type = list(any)
}
 