resource "aws_subnet" "ossec_pvt_subnets" {
  count                   = length(var.pvt_subnet_Name)
  vpc_id                  = var.vpc_id
  cidr_block              = var.pvt_subnet_CIDR[count.index]
  availability_zone       = var.private_available_zone[count.index]
  map_public_ip_on_launch = false
  tags = merge(
    var.additional_tags,
    {
      Name = "${var.pvt_subnet_Name[count.index]}"
    },
  )
}



resource "aws_eip" "ossec_eip" {
  count = 1
}

resource "aws_nat_gateway" "ossec_NAT_GW" {
  allocation_id = aws_eip.ossec_eip[0].id
  subnet_id     = var.ossec_pub_subnet_id

  tags = merge(
    var.additional_tags,
    {
      Name = var.nat_gw_name
    },
  )
}


resource "aws_route_table" "ossec_pvt_rt" {
  vpc_id = var.vpc_id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ossec_NAT_GW.id

  }

  tags = merge(
    var.additional_tags, {
      Name = var.pvt_route_table_name
    },
  )
}

resource "aws_route_table_association" "associate_private_subnets" {
  count          = length(var.pvt_subnet_Name)
  subnet_id      = element(aws_subnet.ossec_pvt_subnets.*.id, count.index)
  route_table_id = aws_route_table.ossec_pvt_rt.id
}