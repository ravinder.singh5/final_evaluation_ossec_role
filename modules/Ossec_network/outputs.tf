output "pvt_subnet_id_output" {
  value = aws_subnet.ossec_pvt_subnets.*.id
}