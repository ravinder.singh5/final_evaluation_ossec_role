variable "vpc_id" {
  type = string
}

variable "pvt_subnet_Name" {
  type = list(any)
}

variable "pvt_subnet_CIDR" {
  type = list(any)
}

variable "private_available_zone" {
  type = list(any)
}

variable "additional_tags" {
 type    = map(any)
}

variable "ossec_pub_subnet_id" {
  type = string
}

variable "nat_gw_name" {
  type = string
}

variable "pvt_route_table_name" {
  type = string
}