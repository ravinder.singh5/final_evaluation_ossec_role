variable "vpc_id" {
  type = string
}

variable "target_group_name" {
  type = string
}

variable "target_instance_id" {
  type = string
}

variable "alb_sg_cidr" {
  type = list(any)
}

variable "alb_name" {
  type = string
}

variable "alb_subnets" {
  type = list(any)
}
 