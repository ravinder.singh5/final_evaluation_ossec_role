variable "vpc_id" {
  type = string
}

variable "private_sg_name" {
  type = string
}

variable "ports" {
  type = list(any)
}

variable "protocol" {
  type = string
}

variable "ossec_server_instance_name" {
  type = string
}

variable "pvt_subnet_id" {
  type = list(any)
}

variable "instance_ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "key_name" {
  type = string
}

variable "ossec_server_additional_tags" {
  default = {}
  type    = map(any)
}

variable "agent_sg_name" {
  type = string
}

variable "agent_instances_name" {
  type = list(any)
}

variable "ossec_agent_additional_tags" {
  default = {}
  type = map(any)
}








  
