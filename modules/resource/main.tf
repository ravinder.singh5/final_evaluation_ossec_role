resource "aws_security_group" "ossec_server_security_group" {
  vpc_id = var.vpc_id
  name   = var.private_sg_name
  dynamic "ingress" {
    for_each = var.ports
    content {
      from_port   = 0 #ingress.value
      to_port     = 65535 #ingress.value
      protocol    = var.protocol
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
  }
}



resource "aws_instance" "pub_ec2_instance" {
  count           = 1
  subnet_id       = var.pvt_subnet_id[count.index]
  ami             = var.instance_ami
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = [aws_security_group.ossec_server_security_group.id]
  ebs_optimized   = false
  user_data       = <<EOF
#!/bin/bash
git clone https://gitlab.com/ravinder.singh5/ossec_installation.git
mv ossec_installation/ossec.sh /home/ubuntu/
cd /home/ubuntu
sudo chmod +x ossec.sh
printf '\n\nen\nserver\n\nn\ny\ny\ny\ny\nn\ny\n\n' | sudo ./ossec.sh
EOF
  tags = merge(
    var.ossec_server_additional_tags,
    {
      Name = var.ossec_server_instance_name
    },
  )
}

############################### agents ####################################

resource "aws_security_group" "ossec_agent_security_group" {
  vpc_id = var.vpc_id
  name   = var.agent_sg_name
  dynamic "ingress" {
    for_each = var.ports
    content {
      from_port   = 0 #ingress.value
      to_port     = 65535 #ingress.value
      protocol    = var.protocol
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
  }
}


resource "aws_instance" "agent_ec2_instance" {
  count           = length(var.agent_instances_name)
  subnet_id       = var.pvt_subnet_id[count.index]
  ami             = var.instance_ami
  instance_type   = var.instance_type
  key_name        = var.key_name
  security_groups = [aws_security_group.ossec_agent_security_group.id]
  ebs_optimized   = false
  tags = merge(
    var.ossec_agent_additional_tags,
    {
      Name = var.agent_instances_name[count.index]
    },
  )
}