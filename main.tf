provider "aws" {
  region  = var.region
}

module "ossec_infra" {
  source                 = "./modules/Ossec_network"
  vpc_id                 = var.vpc_id
  pvt_subnet_Name        = var.pvt_subnet_Name
  pvt_subnet_CIDR        = var.pvt_subnet_CIDR
  private_available_zone = var.private_available_zone
  additional_tags        = var.additional_tags
  ossec_pub_subnet_id    = var.ossec_pub_subnet_id
  nat_gw_name            = var.nat_gw_name
  pvt_route_table_name   = var.pvt_route_table_name
}


module "ossec_resource" {
  source                       = "./modules/resource"
  vpc_id                       = var.vpc_id
  private_sg_name              = var.private_sg_name
  ports                        = var.ports
  protocol                     = var.protocol
  ossec_server_instance_name   = var.ossec_server_instance_name
  pvt_subnet_id                = module.ossec_infra.pvt_subnet_id_output
  instance_ami                 = var.instance_ami
  instance_type                = var.instance_type
  key_name                     = var.key_name
  ossec_server_additional_tags = var.ossec_server_additional_tags

  agent_sg_name               = var.agent_sg_name
  agent_instances_name        = var.agent_instances_name
  ossec_agent_additional_tags = var.ossec_agent_additional_tags
}

module "ossec_loadbalancer" {
  source             = "./modules/load_balancer"
  vpc_id             = var.vpc_id
  target_group_name  = var.target_group_name
  target_instance_id = module.ossec_resource.ossec_server_instanceID_output
  alb_sg_cidr        = var.alb_sg_cidr
  alb_name           = var.alb_name
  alb_subnets        = var.alb_subnets
}


terraform {
  backend "s3" {
    bucket = "ossec-terra-bucket"
    key    = "terraform/terraform.tfstate"
    region = "ap-northeast-1"
  }
}