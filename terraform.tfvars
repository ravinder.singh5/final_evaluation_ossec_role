region = "ap-northeast-1"

vpc_id                 = "vpc-01017829671937801"
pvt_subnet_Name        = ["Ossec_pvt_sn1", "Ossec_pvt_sn2"]     #"Ossec_pvt_sn3", "Ossec_pvt_sn4"]
pvt_subnet_CIDR        = ["10.0.16.0/22", "10.0.20.0/22"]       #"10.0.24.0/22", "10.0.28.0/22"]
private_available_zone = ["ap-northeast-1a", "ap-northeast-1c"] #, "ap-northeast-1d", "ap-northeast-1a"]
additional_tags        = { tool = "ossec" }

ossec_pub_subnet_id = "subnet-012410d4958c1dbb5"
nat_gw_name         = "ossec_nat_gw"

pvt_route_table_name = "ossec-pvt-rt"
private_sg_name      = "ossec-server-sg"
ports                = ["0" - "65535"]
protocol             = "all"

ossec_server_instance_name   = "ossec-server"
instance_ami                 = "ami-0a3eb6ca097b78895"
instance_type                = "t2.micro"
key_name                     = "ekl"
ossec_server_additional_tags = { tool = "ossec", type = "server" }


agent_sg_name               = "ossec-agent-sg"
agent_instances_name        = ["agent-1", "agent-2"] # "agent-3", "agent-4"]
ossec_agent_additional_tags = { tool = "ossec", type = "agent" }


target_group_name = "ossec-server-tg"
alb_sg_cidr       = ["0.0.0.0/0"]
alb_name          = "ossec-load-balancer"
alb_subnets       = ["subnet-012410d4958c1dbb5", "subnet-04484fe876eac2311"]


